use rocket::fs::FileServer;
use rocket::launch;
use std::env;

#[launch]
fn rocket() -> _ {
    dotenv::dotenv().ok();
    rocket::build().mount(
        "/",
        FileServer::from(env::var("DIST_DIR").unwrap_or_else(|_| "/dist".into())),
    )
}

#[cfg(test)]
mod tests {
    #[test]
    fn back_should_work() {
        assert_eq!(1 + 1, 2);
    }
}
