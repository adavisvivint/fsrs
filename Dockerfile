FROM zwo1in/fullstack-rs:latest AS builder

WORKDIR /app
COPY . .
RUN cargo install --path back


FROM debian:11.1-slim

WORKDIR /app
ENV DIST_DIR=/dist

COPY --from=builder /app/front/dist $DIST_DIR
COPY --from=builder /usr/local/cargo/bin/back .
COPY .env.sample .env

CMD ./back
