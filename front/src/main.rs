use yew::prelude::*;

enum Msg {
    AddOne,
}

struct Model {
    // `ComponentLink` is like a reference to a component.
    // It can be used to send messages to the component
    link: ComponentLink<Self>,
    value: i64,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self { link, value: 0 }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::AddOne => {
                self.value += 1;
                // the value has changed so we need to
                // re-render for it to appear on the page
                true
            }
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
            <>
                { logo() }
                { heading() }
                <div class="panel">
                    <p>{ self.value }</p>
                    <button onclick=self.link.callback(|_| Msg::AddOne)>{ "+1" }</button>
                </div>
            </>
        }
    }
}

fn heading() -> Html {
    html! {
        <div class="heading">
            <h1>{ "Fullstack Rust application" }</h1>
            <h3>{ "Built with Yew and Rocket" }</h3>
        </div>
    }
}

fn logo() -> Html {
    let alt = "Rust language official logo";
    let src = "/static/Rust_programming_language_black_logo.svg";
    html! {
        <img class="logo" src=src alt=alt />
    }
}

fn main() {
    yew::start_app::<Model>();
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use wasm_bindgen::JsCast;
    use wasm_bindgen_test::wasm_bindgen_test;
    use yew::web_sys::HtmlElement;

    // This macro should be only called once per binary in crate
    wasm_bindgen_test::wasm_bindgen_test_configure!(run_in_browser);

    #[wasm_bindgen_test]
    fn clicking_on_button_should_increment_value() {
        let document = yew::utils::document();
        let body = document.create_element("div").unwrap();
        yew::initialize();
        App::<Model>::new().mount(body.clone());

        let value = body.query_selector(".panel > p").unwrap().unwrap();
        let button = body.query_selector(".panel > button").unwrap().unwrap();
        let button = button.dyn_into::<HtmlElement>().unwrap();

        assert_eq!("0", value.text_content().unwrap());
        button.click();
        assert_eq!("1", value.text_content().unwrap());
    }
}
